import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const httpHeaders = new HttpHeaders({
  'content-Type': 'application/json'
});

const httpOptions = {
  headers: httpHeaders
};
@Injectable({
  providedIn: 'root'
})
export class CountriesService {
public url = 'https://raw.githubusercontent.com/sagarshirbhate/Country-State-City-Database/master/Contries.json';
  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }
  // Calling Countries

  allCountries(): Observable<any> {
    return this.http.get(this.url)
      .pipe(
        catchError(this.handleError)
      );
  }

}
