import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginsuranceComponent } from './loginsurance.component';

describe('LoginsuranceComponent', () => {
  let component: LoginsuranceComponent;
  let fixture: ComponentFixture<LoginsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
