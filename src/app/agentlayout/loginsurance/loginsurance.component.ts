import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AgentService } from '../../../agent.service';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import * as crypto from 'crypto-js';
import swal from 'sweetalert2';

@Component({
  selector: 'app-loginsurance',
  templateUrl: './loginsurance.component.html',
  styleUrls: ['./loginsurance.component.scss']
})
export class LoginsuranceComponent implements OnInit {
  public loader : Boolean = false;
  submitted = false;
  writePolicyform: FormGroup;
  insurancecompany: any;
  companyproducts: any;
  inscompanyname: string;
  product: string;
  premiumfreq: string;
  premiumamount: string;
  policy: any;

  outinscompanyname: string;
  outproduct: string;
  outinsuranceid: string;
  clock: Date;
  outpremiumfreq: string;
  outpremiumamount: number;
  outyearpremium: number;
  outagentcommission: number;
  outcompanypool: number;

  showSwal(type) {
    if (type == 'logSuccess') {
      swal({
        title: '',
        text: 'Policy has been created Successfully!',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Something went wrong!',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    } else if (type == 'successLic') {
      swal({
        title: '',
        text: 'Licence Upload Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    }
  }

  constructor(private formBuilder: FormBuilder, private cookieService: CookieService,
    private agent: AgentService, private router: Router) { }

  ngOnInit() {

    // Validations start

    this.writePolicyform = this.formBuilder.group({

      inscompanyname: [null, [Validators.required]],
      product: [null, [Validators.required]],
      insuranceid: [null, [Validators.required]],
      premiumfreq: [null, [Validators.required]],
      premiumamount: [null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
    });

    let id = null;
    let agentData = this.cookieService.get('AXRGBSYEQWATNLKFSYT') ? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')) : null;
    id = agentData != null ? agentData.agentId : null;
    let xy = [];

    if (id != null) {

      this.agent.getAllInsOnlog(id)
        .subscribe(res => {
    console.log('res.data.MainData res.data.MainData',res.data.MainData);
          //console.log('resr res', res);
          //console.log('data is on');
          if (res.data.MainData.length != 0) {
            res.data.MainData.map(ins => {
              //console.log(ins.companyName);
              //console.log(ins.companyProductNames);
              xy.push({ 'companyname': ins.companyName, 'products': ins.companyProducts })
            })
          }
          //console.log(xy);
          this.insurancecompany = xy;

        }, (err) => {
          //console.log(err);
        });
    }

    this.clock = new Date();

    this.writePolicyform = this.formBuilder.group({
      inscompanyname: [null, [Validators.required]],
      product: [null, [Validators.required]],
      insuranceid: [null, [Validators.required]],
      premiumfreq: [null, [Validators.required]],
      premiumamount: [null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
  }
  // Form Controls
  get f() { return this.writePolicyform.controls; }
  companySelect() {
    //console.log('shyam')
    this.policy = {
      inscompanyname: this.writePolicyform.get('inscompanyname').value.trim(),
    };

    console.log('shyam', this.policy.inscompanyname)
    console.log('shyam', this.insurancecompany)
    this.insurancecompany.map(c => {
      // //console.log(c);
      if (c.companyname === this.policy.inscompanyname) {
        console.log('c.products', c.products)
        this.companyproducts = c.products;
      }
    })
    this.outinscompanyname = this.policy.inscompanyname;

  }

  productSelect() {
    //console.log('kumar');
    this.policy = {
      product: this.writePolicyform.get('product').value.trim(),
    };
    //console.log('shyam', this.policy.product)
    this.outproduct = this.policy.product;
  }

  policyId() {
    this.policy = {
      insuranceid: this.writePolicyform.get('insuranceid').value.trim(),
    };
    this.outinsuranceid = this.policy.insuranceid;
  }

  prefreq() {
    this.policy = {
      premiumfreq: this.writePolicyform.get('premiumfreq').value.trim(),
    };
    this.outpremiumfreq = this.policy.premiumfreq;
    if (this.writePolicyform.get('premiumamount').value != null) {
      this.enterAmount();
    }
  }

  enterAmount() {
    this.policy = {
      insuranceCompanyname: this.writePolicyform.get('inscompanyname').value.trim(),
      productName: this.writePolicyform.get('product').value.trim(),
      insuranceId: this.writePolicyform.get('insuranceid').value.trim(),
      premiumFrequency: this.writePolicyform.get('premiumfreq').value.trim(),
      premiumamount: this.writePolicyform.get('premiumamount').value,
    };

    this.outpremiumamount = parseFloat(this.policy.premiumamount);
    this.outyearpremium = this.outpremiumfreq == 'Weekly' ? parseFloat(this.policy.premiumamount) * 52 : this.outpremiumfreq == 'Monthly' ? parseFloat(this.policy.premiumamount) * 12 : parseFloat(this.policy.premiumamount);
    this.outagentcommission = (this.outyearpremium * 80) / 100;
    this.outcompanypool = this.outyearpremium - this.outagentcommission;

    this.policy.premiumAmount = this.outpremiumamount;
    this.policy.yearPremiumAmount = this.outyearpremium;
    this.policy.agentCommissionAmount = this.outagentcommission;
    this.policy.companyPoolAmount = this.outcompanypool;
  }
  // Form Submission
  onPolicyWrite(policy) {
    this.submitted = true;

    //console.log('Write Policy', this.policy);
    if (this.writePolicyform.invalid) {
      //console.log('please fill all fields');
      return;
    }
    this.loader = true;
    //console.log('shyam kumar')
    let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

    this.policy.agentId = agentdata.agentId;
    this.policy.sponserId = agentdata.sponserId;

    //console.log('this.policy', this.policy, agentdata)

    this.agent.writePolicy(this.policy)
      .subscribe(res => {
        // this.showSwal('logSuccess')
        this.loader = false;
        this.router.navigateByUrl('/maf-agent/view-insurance')
      }, (err) => {
        this.loader = false;
        this.showSwal('fail')
        //console.log(err);
      });
  }


}
