import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from '../../../projects/ngx-paypal-lib/src/public_api';
import { AgentService } from '../../../agent.service';


import { SidebarComponent } from './../sidebar/sidebar.component';


import { CookieService } from 'ngx-cookie-service';
import { GlobalValueService } from './../../global-value.service';



import swal from 'sweetalert2';



@Component({
  selector: 'app-errorinfo',
  templateUrl: './errorinfo.component.html',
  styleUrls: ['./errorinfo.component.scss']
})
export class ErrorinfoComponent implements OnInit {

  public loader: Boolean = false;
  public payPalConfig?: PayPalConfig;
  s3AssectUrl: any;
  EandOPayment: any;
  agentDetails: any;
  PaymentDueDetails: any;
  firstName: string;
  emailId: string;
  amountToPay: string;
  monthlyPayment: boolean;
  paypalbutton: boolean;
  phoneNo: string;
  agentId: string;
  tempAgentData: any;
  TotalAmounttopay: any;
  finalamount: any;
  MonthDetails: any;
  EandOpaymentList: any;

  showSwal(type) {
    if (type == 'patmentDone') {
      swal({
        title: '',
        text: 'Payment Done Successfully!',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Something went wrong!',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
  }

  constructor(private router: Router, private agent: AgentService, private cookieService: CookieService, private global: GlobalValueService,
    private sidebar: SidebarComponent) { }

  ngOnInit() {
    this.s3AssectUrl = `${this.global.s3Url}`;

    console.log('SidebarComponentSidebarComponent', this.sidebar.onInitializerOfSideBar)
    this.onEandOInitialize();
  }


  onEandOInitialize() {
    this.loader = true;
    let id = null;
    this.EandOPayment = {};
    const agentIdCo = localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser');
    const gatData = this.cookieService.get('AXRGBSYEQWATNLKFSYT') ? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')) : null;
    id = gatData != null ? gatData.agentId : null;
    if (id != null && id == agentIdCo) {
      let data = { agentId: id }
      this.agent.getAgentProfile(data)
        .subscribe(res => {
          console.log('resr res', res);
          console.log('data is on');
          this.cookieService.set('THJSIDGTFRCDHAFCSKDHDGDTPR', JSON.stringify(res.data));
          this.agentDetails = JSON.parse(this.cookieService.get('THJSIDGTFRCDHAFCSKDHDGDTPR'));
          this.monthlyPayment = this.agentDetails.monthlyPayment;
          this.firstName = this.agentDetails.firstName;
          this.phoneNo = this.agentDetails.phoneNo;
          this.emailId = this.agentDetails.emailId;
          this.agentId = this.agentDetails.agentId;
          console.log('this.monthlyPayment this.monthlyPayment',this.monthlyPayment)
          if (this.monthlyPayment == false) {
          	this.paypalbutton = true;
          	if(this.agentDetails.PaymentDueDetails != undefined)
          	{
	            this.PaymentDueDetails = this.agentDetails.PaymentDueDetails;
	            this.TotalAmounttopay = this.PaymentDueDetails.TotalAmount;
	            this.finalamount = parseInt(this.TotalAmounttopay);
	            this.EandOPayment.NumberOfMonthsPaymentDue = this.PaymentDueDetails.NumberOfMonthsPaymentDue;
	            this.EandOPayment.MonthDetails = this.PaymentDueDetails.MonthDetails;
	            this.EandOPayment.emailId = this.PaymentDueDetails.emailId;
	            this.EandOPayment.agentId = this.PaymentDueDetails.agentId;
	            this.EandOPayment.TotalAmounttopay = '$' + this.TotalAmounttopay;
	            this.initConfig();
        	}
            this.loader = false;
          }
          else {
          	this.paypalbutton = false;
          	this.sidebar.onInitializerOfSideBar();
            this.finalamount = 0;
            this.loader = false;
          }
        }, (err) => {
          console.log(err);
          this.loader = false;
        });

      this.agent.EandOAgentPaymentList(data)
        .subscribe(res => {
          console.log('resr res Two', res);
          this.EandOpaymentList = res.data.MainData;
        }, (err) => {
          console.log(err);
          this.loader = false;
        });
    }
    else {
      this.router.navigateByUrl('/maf-agent/agent-login');
    }

  }

  private initConfig(): void {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
    }, 4000);
	    this.payPalConfig = new PayPalConfig(
	      PayPalIntegrationType.ClientSideREST,
	      PayPalEnvironment.Sandbox,
	      {
	        commit: true,
	        client: {
	          sandbox:
	            'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
	        },
	        button: {
	          label: 'paypal',
	          layout: 'vertical'
	        },
	        onAuthorize: (data, actions) => {
	          console.log('Authorize');
	          return of(undefined);
	        },
	        onPaymentComplete: (data, actions) => {
	          this.loader = true;
	          let final_e_and_o_obj = new Object();
	          this.EandOPayment.intent = data.intent;
	          this.EandOPayment.orderId = data.orderID;
	          this.EandOPayment.payerId = data.payerID;
	          this.EandOPayment.paymentId = data.paymentID;
	          this.EandOPayment.paypalCode = data.paymentID;
	          this.EandOPayment.paymentToken = data.paymentToken;
	          this.EandOPayment.paymentDate = new Date().toISOString();
	          final_e_and_o_obj = this.EandOPayment;
	          console.log('final_e_and_o_obj final_e_and_o_obj', final_e_and_o_obj)

	          this.agent.EandOPostPayment(final_e_and_o_obj)
	            .subscribe(res => {
	              this.loader = false;
	              this.onEandOInitialize();
	              this.showSwal('patmentDone')
	            }, (err) => {
	              console.log(err);
	              this.loader = false;
	              this.showSwal('fail')
	              this.onEandOInitialize();
	            });
	        },
	        onCancel: (data, actions) => {
	          console.log('OnCancel');
	        },
	        onError: err => {
	          console.log('OnError OnError', err);
	        },
	        onClick: () => {
	          console.log('onClick');
	        },
	        validate: (actions) => {
	          console.log(actions);
	        },
	        experience: {
	          noShipping: true,
	          brandName: 'MAF PAYMEMTS'
	        },
	        transactions: [
	          {
	            amount: {
	              total: this.TotalAmounttopay,
	              currency: 'USD',
	              details: {
	                subtotal: this.TotalAmounttopay,
	                tax: 0.00,
	                shipping: 0.00,
	                handling_fee: 0.00,
	                shipping_discount: 0.00,
	                insurance: 0.00
	              }
	            },
	            custom: 'Custom value',
	            item_list: {
	              items: [
	                {
	                  name: 'MAF SUBSCRIPTION',
	                  description: 'THE AMOUNT FOR MAF SUBSCRIPTION.',
	                  quantity: 1,
	                  price: this.finalamount,
	                  tax: 0.00,
	                  sku: '1',
	                  currency: 'USD'
	                }],
	              // shipping_address: {
	              //   recipient_name: 'Brian Robinson',
	              //   line1: '4th Floor',
	              //   line2: 'Unit #34',
	              //   city: 'San Jose',
	              //   country_code: 'US',
	              //   postal_code: '95131',
	              //   phone: '011862212345678',
	              //   state: 'CA'
	              // },
	            },
	          },
	        ],
	        payment: () => {
	          // create your payment on server side
	          return of(undefined);
	        },
	        note_to_payer: 'Contact us if you have troubles processing payment'
	      }
	    );
  }

  // private prettify(): void {
  //   hljs.initHighlightingOnLoad();
  // }
  goToHome() {
    this.router.navigateByUrl('/home');
  }

}
