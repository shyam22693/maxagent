import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { AuthorizationService } from '../../customservices/authorization.service';

import { CookieService } from 'ngx-cookie-service';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';
import swal from 'sweetalert2';

const poolData = {
  UserPoolId: 'us-east-1_B5waepwSZ', // Your user pool id here
  ClientId: '4gkgpcug955h6kfapt4n61ojkr' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);

@Component({
  selector: 'app-agentlogin',
  templateUrl: './agentlogin.component.html',
  styleUrls: ['./agentlogin.component.scss']
})
export class AgentloginComponent implements OnInit {

  public loader: Boolean = false;
  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  subbutton: boolean = false;
  error: string = '';
  agentId: string;
  submitted = false;
  user: any;
  agentloginform: FormGroup;
  bAuthenticated = false;
  public focus;
  public focus2;
  showSwal(type) {
    if (type == 'success') {
      swal({
        title: '',
        text: 'Login Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop).then(()=> {
        this.gotodashboard();
      })
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Username or Password Mismatch',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
  }
  constructor(private formBuilder: FormBuilder, private agentAuth: AuthorizationService,
    public router: Router, private activatedRoute: ActivatedRoute, private cookieService: CookieService) { }
  ngOnInit() {

    this.agentloginform = this.formBuilder.group({
      agentId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });

    // Background

    var $page = document.getElementsByClassName('full-page')[0];
    var image_src;
    var image_container = document.createElement('div');
    image_container.classList.add('full-page-background');
    image_container.style.backgroundImage = 'url(https://s3.amazonaws.com/mafserverless/bg14.jpg  )';
    $page.appendChild(image_container);
    $page.classList.add('login-page');


    let authenticatedUser = this.agentAuth.getAuthenticatedUser();

    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }
  // onDestroy

  ngOnDestroy() {
    var $page = document.getElementsByClassName('full-page')[0];
    $page.classList.remove('login-page');
  }
  get ad() { return this.agentloginform.controls; }
  onLogin(user) {


    this.user = {
      agentid: this.agentloginform.get('agentId').value,
      password: this.agentloginform.get('password').value
    };

    this.submitted = true;
    // this.loader = true;
    if (this.agentloginform.invalid) {
      return;
    }
    else{
        this.loader = true;
        
    }

    let agentid = this.user.agentid;
    let password = this.user.password;
    this.agentAuth.signIn(agentid, password).subscribe(
      (data) => {
        // $('.login-btn').attr('data-dismiss', 'modal');
        this.confirmCode = false;
        let cognitoUser = userPool.getCurrentUser();
        if (cognitoUser != null) {
          let cognitoUser = userPool.getCurrentUser();
          let agentid = 'dummy';
          cognitoUser.getSession((err, session) => {
            cognitoUser.getUserAttributes((err, result) => {
              if (result[0].getValue()) {
                let agentId = { 'agentId': result[0].getValue() };
                this.cookieService.set('AXRGBSYEQWATNLKFSYT', JSON.stringify(agentId));
                this.loader = false;
                this.showSwal('success');
                // this.router.navigateByUrl('/maf-agent/agent-dashboard');
              }
            });

          });
        }
      },
      (err) => {

        this.showSwal('fail');
        this.loader = false;

        this.error = 'Registration Error has occurred';
      }
    );
  }

  gotodashboard()
  {
    this.router.navigateByUrl('/maf-agent/agent-dashboard');
  }



}

