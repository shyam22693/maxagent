import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AgentlayoutRoutingModule } from './agentlayout-routing.module';
import { AgentlayoutComponent } from './agentlayout.component';

import { AgentdashboardComponent } from './agentdashboard/agentdashboard.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AgentloginComponent } from './agentlogin/agentlogin.component';

import { NgxPayPalModule } from '../../projects/ngx-paypal-lib/src/public_api';

import { LoginComponent } from './login/login.component';

import { AgentprofileComponent } from './agentprofile/agentprofile.component';
import { LoginsuranceComponent } from './loginsurance/loginsurance.component';
import { ViewinsuranceComponent } from './viewinsurance/viewinsurance.component';
import { ErrorinfoComponent } from './errorinfo/errorinfo.component';
import { InsuranceinfoComponent } from './insuranceinfo/insuranceinfo.component';
import { PdfViewerModule } from 'ng2-pdf-viewer'; // <- import OrderModule

import { ChartsModule } from 'ng2-charts';
import { AgentbusinessComponent } from './agentbusiness/agentbusiness.component';
@NgModule({
  imports: [
    CommonModule,
    AgentlayoutRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PdfViewerModule,
    NgxPayPalModule,
    ChartsModule
  ],
  declarations: [AgentlayoutComponent, AgentdashboardComponent,

    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    AgentloginComponent,

    LoginComponent,
    AdminLayoutComponent, AuthLayoutComponent,

    AgentprofileComponent, LoginsuranceComponent,
    ViewinsuranceComponent, ErrorinfoComponent, InsuranceinfoComponent, AgentbusinessComponent,


  ],
  exports: [
    AgentlayoutComponent, AgentdashboardComponent,

    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    AdminLayoutComponent, AuthLayoutComponent,

    AgentprofileComponent, LoginsuranceComponent,
    ViewinsuranceComponent, ErrorinfoComponent,
    InsuranceinfoComponent

  ],providers:[SidebarComponent]
})
export class AgentlayoutModule { }
