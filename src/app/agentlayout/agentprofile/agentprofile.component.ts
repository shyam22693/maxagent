import { Component, DoCheck, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from '../../customservices/adminauthorization.service';
import { AuthorizationService } from '../../customservices/authorization.service';
import swal from 'sweetalert2';
import { DomSanitizer } from '@angular/platform-browser';


const poolData = {
  UserPoolId: 'us-east-1_mHJWVEl15', // Your user pool id here
  ClientId: '3ahsjgh8mrhf4dvgfsoujaaisl' // Your client id here
};

const userPool = new CognitoUserPool(poolData);

@Component({
  selector: 'app-agentprofile',
  templateUrl: './agentprofile.component.html',
  styleUrls: ['./agentprofile.component.scss']
})
export class AgentprofileComponent implements DoCheck, OnInit {
  //  src = 'https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf';
  // src = 'https://cors-anywhere.herokuapp.com/https://mafserverless.s3.amazonaws.com/protected/AAA14/agentLicence/licence_20190321T070832Z.pdf';
  // src = {
  //       'https://mafserverless.s3.amazonaws.com/protected/AAA14/agentLicence/licence_20190321T070832Z.pdf',

  //      }
  withCredentials: true;
  acceptedMimeTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/jpg'];


  acceptedLicenceMimeTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/jpg',
    'application/pdf'];

  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('fileLicenceInput') fileLicenceInput: ElementRef;

  url = '';
  agentDetails: any;
  fileDataUri: string | ArrayBuffer;
  fileLicenceDataUri: string | ArrayBuffer;
  errorMsg: string;
  errorLicenceMsg: string;
  fileName: string;
  fileLicenceName: string;
  fileType: string;
  fileLicenceType: string;
  imageUpload: boolean;
  imageLicenceUpload: boolean;
  LicenceURI: string;
  licenceThere: any;
  LicenceUploaded: boolean;
  pdfViewer: Boolean = false;
  loader: Boolean = false;
  licence: string;
  LicenceURL: any;
  showSwal(type) {
    if (type == 'successPic') {
      swal({
        title: '',
        text: 'Profile Pic Upload Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Something went wrong!',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    } else if (type == 'successLic') {
      swal({
        title: '',
        text: 'Licence Upload Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    }
    else if (type == 'NoLicence') {
          swal({
            title: '',
            text: 'No licence uploaded!',
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-info'
          }).catch(swal.noop);
        }
  }
  constructor(private sanitizer: DomSanitizer, private agent: AgentService, private agentAuth: AuthorizationService,
    private router: Router, private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }

  ngDoCheck() {
    this.agentDetails = JSON.parse(this.cookieService.get('THJSIDGTFRCDHAFCSKDHDGDTPR'));
    // let data = { agentId: this.agentDetails.agentId };
    // this.agent.getAgentProfile(data)
    //   .subscribe(res => {
    //     console.log('resr res', res);
    //     console.log('data is on');
    //     this.cookieService.set('THJSIDGTFRCDHAFCSKDHDGDTPR', JSON.stringify(res.data));
    //     this.agentDetails = JSON.parse(this.cookieService.get('THJSIDGTFRCDHAFCSKDHDGDTPR'));
    //     this.LicenceURI = this.agentDetails.LicenceURI? 'https://cors-anywhere.herokuapp.com/'+this.agentDetails.LicenceURI :this.agentDetails.LicenceURI;
    //     this.loader = false;
    //   }, (err) => {
    //     console.log(err);
    //   });
  }

  ngOnInit() {
    this.onProfileInitialize();
  }

  onProfileInitialize()
  {
    this.loader = true;
    this.errorMsg = '';
    this.errorLicenceMsg = '';
    this.imageUpload = false;
    this.imageLicenceUpload = false;
    let id = null;
    const agentIdCo = localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser');
    const gatData = this.cookieService.get('AXRGBSYEQWATNLKFSYT') ? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')) : null;
    id = gatData != null ? gatData.agentId : null;
    if (id != null && id == agentIdCo) {
      let data = { agentId: id }
      console.log('data', data);

      this.agent.getAgentProfile(data)
        .subscribe(res => {
          console.log('resr res', res);
          console.log('data is on');
          this.cookieService.set('THJSIDGTFRCDHAFCSKDHDGDTPR', JSON.stringify(res.data));
          this.agentDetails = JSON.parse(this.cookieService.get('THJSIDGTFRCDHAFCSKDHDGDTPR'));
          // this.LicenceURI = 
          // this.agentDetails.LicenceURI ? 'https://cors-anywhere.herokuapp.com/' + this.agentDetails.LicenceURI : this.agentDetails.LicenceURI;
          this.LicenceUploaded = res.data;
          this.licence = this.agentDetails.LicenceURI;
          this.licenceThere = this.licence ? true : false; 
          console.log('LicenceUploaded', res.data.LicenceUploaded);
          this.pdfViewer = this.LicenceUploaded;
          this.loader = false;
        }, (err) => {
          console.log(err);
          this.loader = false;
        });
    }
    else {
      this.router.navigateByUrl('/maf-agent/agent-login');
    }

  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (Event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      };
    }
  }


  previewFile() {
    this.loader = true;
    const file = this.fileInput.nativeElement.files[0];
    console.log('file file', file);
    if (file && this.acceptedMimeTypes.includes(file.type) && file.size < 5000000) {
      this.loader = true;
      this.fileName = file.name;
      this.fileType = file.type;
      this.imageUpload = true;
      const reader = new FileReader();
      reader.readAsDataURL(this.fileInput.nativeElement.files[0]);
      reader.onload = () => {
        this.fileDataUri = reader.result;
        let base64File = this.fileDataUri.toString().split(',')[1];
        let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));
        let data = { base64File: base64File, agentId: agentdata.agentId, type: 'image', fileType: this.fileType, fileName: this.fileName }
        console.log('data in preview', data);
        this.agent.uploadProfilePic(data)
          .subscribe(res => {
            this.showSwal('successPic');
            this.onProfileInitialize();
            this.loader = false;
          }, (err) => {
            console.log(err);
            this.showSwal('fail');
            this.onProfileInitialize();
            this.loader = false;
          });
      }
    }
    else {
      this.errorMsg = 'File must be jpg, jpeg, or png and cannot be exceed 500 KB in size'
    }
  }
  // getFileID

  previewLicenceFile() {
    const file = this.fileLicenceInput.nativeElement.files[0];

    console.log('file file', file);

    if (file && this.acceptedLicenceMimeTypes.includes(file.type) && file.size < 5000000) {
      this.loader = true;
      this.fileLicenceName = file.name;
      this.fileLicenceType = file.type;
      this.imageLicenceUpload = true;
      const reader = new FileReader();
      reader.readAsDataURL(this.fileLicenceInput.nativeElement.files[0]);
      reader.onload = () => {
        this.fileLicenceDataUri = reader.result;
        const base64LicenceFile = this.fileLicenceDataUri.toString().split(',')[1];
        let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

        let dataLicence = {
          base64File: base64LicenceFile,
          agentId: agentdata.agentId, type: 'licence', fileType: this.fileLicenceType, fileName: this.fileLicenceName
        }
        this.agent.uploadAgentLicence(dataLicence)
          .subscribe(res => {
            this.showSwal('successLic');
            this.onProfileInitialize();
            this.loader = false;
          }, (err) => {
            console.log(err);
            this.showSwal('fail');
            this.onProfileInitialize();
            this.loader = false;
          });
      }
    } else {
      this.errorLicenceMsg = 'File must be jpg, jpeg, png, or pdf and cannot be exceed 500 KB in size';
    }
  }

  getFileId() {
    document.getElementById('input-file').click();
  }


  validateFile(file) {
    return this.acceptedMimeTypes.includes(file.type) && file.size < 5000000;
  }

  validateLicenceFile(file) {
    return this.acceptedLicenceMimeTypes.includes(file.type) && file.size < 5000000;
  }

  writePol() {
    this.router.navigateByUrl('/agent-write-policy');
  }
  /**Download File */
  downloadFile() {
    const data = 'licence';
    const blob = new Blob([data], { type: 'application/octet-stream' });
    if (this.licence) {
      this.LicenceURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.licence);
    }
    else {
      this.showSwal('NoLicence');
    }
  }

}
