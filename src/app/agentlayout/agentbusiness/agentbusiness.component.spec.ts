import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentbusinessComponent } from './agentbusiness.component';

describe('AgentbusinessComponent', () => {
  let component: AgentbusinessComponent;
  let fixture: ComponentFixture<AgentbusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentbusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentbusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
