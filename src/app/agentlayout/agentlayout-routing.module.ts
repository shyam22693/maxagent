import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentlayoutComponent } from './agentlayout.component';
import { AgentdashboardComponent } from './agentdashboard/agentdashboard.component';
import { AgentloginComponent } from './agentlogin/agentlogin.component';

import { LoginComponent } from './login/login.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { AuthGuard } from './../auth.guard';
import { AuthagentGuard } from './../authagent.guard';

import { AgentprofileComponent } from './agentprofile/agentprofile.component';
import { LoginsuranceComponent } from './loginsurance/loginsurance.component';
import { ViewinsuranceComponent } from './viewinsurance/viewinsurance.component';
import { ErrorinfoComponent } from './errorinfo/errorinfo.component';
import { InsuranceinfoComponent } from './insuranceinfo/insuranceinfo.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'agent-login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AgentlayoutComponent,
    children: [
      { path: '', redirectTo: 'agent-dashboard', pathMatch: 'prefix' },
      { path: 'agent-dashboard', component: AgentdashboardComponent},
      { path: 'agent-profile', component: AgentprofileComponent, canActivate: [AuthagentGuard] },
      { path: 'log-insurance', component: LoginsuranceComponent, canActivate: [AuthagentGuard] },
      { path: 'view-insurance', component: ViewinsuranceComponent, canActivate: [AuthagentGuard] },
      { path: 'error-ommission', component: ErrorinfoComponent, canActivate: [AuthagentGuard] },
      { path: 'insurance-info', component: InsuranceinfoComponent, canActivate: [AuthagentGuard] }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: 'agent-login' },
      { path: 'agent-login', component: AgentloginComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentlayoutRoutingModule { }
