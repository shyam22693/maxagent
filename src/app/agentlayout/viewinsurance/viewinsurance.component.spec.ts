import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewinsuranceComponent } from './viewinsurance.component';

describe('ViewinsuranceComponent', () => {
  let component: ViewinsuranceComponent;
  let fixture: ComponentFixture<ViewinsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewinsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewinsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
