import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


import { AgentService } from '../../../agent.service';

@Component({
  selector: 'app-viewinsurance',
  templateUrl: './viewinsurance.component.html',
  styleUrls: ['./viewinsurance.component.scss']
})
export class ViewinsuranceComponent implements OnInit {
  loader: Boolean = true;
  policy: any;
  totalData: any;
  totalCommission: any;
  totalCompanyPool: any;
  constructor(private cookieService: CookieService, private agent: AgentService, private router: Router) { }
  
  ngOnInit() {
    this.totalCommission = 0;
    this.totalCompanyPool = 0;
    let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

    let data = { agentId: agentdata.agentId };

    this.agent.getPolocies(data)
      .subscribe(res => {
        console.log('shyam', res.data.MainData);
        this.totalData = res.data.MainData;

        if (this.totalData.length != 0) {
          this.totalData.map(ins => {
            this.totalCommission += ins.agentCommissionAmount;
            this.totalCompanyPool +=ins.companyPoolAmount;
          });
        }
        this.loader = false;
      },

        (err) => {
          console.log(err);
        });


  }

}
