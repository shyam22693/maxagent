import { ToastrService } from 'ngx-toastr';


import { Component, DoCheck, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from '../../customservices/adminauthorization.service';
import { AuthorizationService } from '../../customservices/authorization.service';



const poolData = {
  UserPoolId: 'us-east-1_mHJWVEl15', // Your user pool id here
  ClientId: '3ahsjgh8mrhf4dvgfsoujaaisl' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);

var misc: any = {
  sidebar_mini_active: true
}

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  isCollapsed?: boolean;
  isCollapsing?: any;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}

export const ROUTES: RouteInfo[] = [
  {
    path: '/maf-agent/agent-dashboard',
    title: 'Dashboard',
    type: 'link',
    icontype: 'media-1_album'
  },
  {
    path: '/maf-agent/agent-profile',
    title: 'Profile',
    type: 'link',
    icontype: 'design_app'
  },
  {
    path: '/maf-agent/error-ommission',
    title: 'E & O',
    type: 'link',
    icontype: 'objects_diamond'
  },
  {
    path: '/maf-agent/log-insurance',
    title: 'Log Insurance',
    type: 'link',
    icontype: 'media-1_album'
  },
  {
    path: '/maf-agent/view-insurance',
    title: 'View Insurance',
    type: 'link',
    icontype: 'business_chart-pie-36'
  }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements DoCheck, OnInit {

  url = '';
  agentDetails: any;
  firstName: string;
  agentId: string;
  sideImage: string;
  LicenceUploaded: any;
  ImageUploaded: any;
  monthlyPayment: any;
  RoutesCustom: any;

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private toastr: ToastrService, private agent: AgentService, private agentAuth: AuthorizationService,
    private router: Router, private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }
  ngDoCheck() {
    // this.agentDetails = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));
    // this.firstName = this.agentDetails.firstName;
    // this.agentId = this.agentDetails.agentId;
    // this.sideImage = this.agentDetails.ImageURI;
  }
  ngOnInit() {
    this.onInitializerOfSideBar();
  }


  onInitializerOfSideBar()
  {
    console.log(ROUTES);
    let id = null;
    const agentId = localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser');
    console.log('agentId agentId', agentId);
    const gatData = this.cookieService.get('AXRGBSYEQWATNLKFSYT') ? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')) : null;
    id = gatData != null ? gatData.agentId : null;
    if (id != null) {
      let data = { agentId: id }
      console.log('data ', data);
      // API Calling
      this.agent.getAgentProfile(data)
        .subscribe(res => {
          console.log('resr resssssssssssssssssssssssssssss', res);
          console.log('data is on');
          this.cookieService.set('AHJSKIDGTERCDHDFCSKDHDGDT', JSON.stringify(res.data));
          this.agentDetails = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));
          this.firstName = this.agentDetails.firstName;
          this.agentId = this.agentDetails.agentId;
          this.sideImage = this.agentDetails.ImageURI;
          this.LicenceUploaded = this.agentDetails.LicenceUploaded;
          this.ImageUploaded = this.agentDetails.ImageUploaded;
          this.monthlyPayment = this.agentDetails.monthlyPayment;
          // this.menuItems = ROUTES.filter(menuItem => menuItem)
          if (this.ImageUploaded == true && this.LicenceUploaded == true && this.monthlyPayment==true) {
            this.menuItems = ROUTES.filter(menuItem => menuItem)
          }
          else {
            this.menuItems = ROUTES.slice(0,3).filter(menuItem => menuItem)
          }
        }, (err) => {
          console.log(err);
        });
    }
    else {
      this.router.navigateByUrl('/maf-agent/agent-login');
    }
    // this.menuItems = ROUTES.filter(menuItem =>
    //   console.log('menuItem', menuItem)
    //   );
  }

  sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if (new Date().getTime() - start > milliseconds) {
        break;
      }
    }
  }
  myFunc(event, menuitem) {
    event.preventDefault();
    event.stopPropagation();
    this.sleep(10);
    if (menuitem.isCollapsing === undefined) {
      menuitem.isCollapsing = true;

      // menuitem.isCollapsed = !menuitem.isCollapsed;

      var element = event.target;
      while (
        element.getAttribute('data-toggle') != 'collapse' &&
        element != document.getElementsByTagName('html')[0]
      ) {
        element = element.parentNode;
      }
      element = element.parentNode.children[1];

      if (
        element.classList.contains('collapse') &&
        !element.classList.contains('show')
      ) {
        element.classList = 'before-collapsing';
        var style = element.scrollHeight;

        element.classList = 'collapsing';
        setTimeout(function () {
          element.setAttribute('style', 'height:' + style + 'px');
        }, 1);
        setTimeout(function () {
          element.classList = 'collapse show';
          element.removeAttribute('style');
          menuitem.isCollapsing = undefined;
        }, 350);
      } else {
        var style = element.scrollHeight;
        setTimeout(function () {
          element.setAttribute('style', 'height:' + (style + 20) + 'px');
        }, 3);
        setTimeout(function () {
          element.classList = 'collapsing';
        }, 3);
        setTimeout(function () {
          element.removeAttribute('style');
        }, 20);
        setTimeout(function () {
          element.classList = 'collapse';
          menuitem.isCollapsing = undefined;
        }, 400);
      }
    }
  }
  minimizeSidebar() {
    const body = document.getElementsByTagName('body')[0];
    if (body.classList.contains('sidebar-mini')) {
      misc.sidebar_mini_active = true
    } else {
      misc.sidebar_mini_active = false;
    }
    if (misc.sidebar_mini_active === true) {
      body.classList.remove('sidebar-mini');
      misc.sidebar_mini_active = false;
      this.showSidebarMessage('Sidebar mini deactivated...');
    } else {
      body.classList.add('sidebar-mini');
      this.showSidebarMessage('Sidebar mini activated...');
      misc.sidebar_mini_active = true;
    }

    // we simulate the window Resize so the charts will get updated in realtime.
    const simulateWindowResize = setInterval(function () {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(function () {
      clearInterval(simulateWindowResize);
    }, 1000);
  }

  showSidebarMessage(message) {
    this.toastr.show(
      `<span class='now-ui-icons ui-1_bell-53'></span>`, message,
      {
        timeOut: 4000,
        closeButton: true,
        enableHtml: true,
        toastClass: 'alert alert-danger alert-with-icon',
        positionClass: 'toast-top-right'
      }
    );
  }

  agentLogout() {
    console.log('shyam');
    this.agentAuth.logOut();
    this.router.navigateByUrl('/maf-agent/agent-login');
  }

  navigateHome() {
    this.router.navigateByUrl('/home');
  }
}
