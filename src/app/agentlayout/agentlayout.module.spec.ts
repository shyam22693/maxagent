import { AgentlayoutModule } from './agentlayout.module';

describe('AgentlayoutModule', () => {
  let agentlayoutModule: AgentlayoutModule;

  beforeEach(() => {
    agentlayoutModule = new AgentlayoutModule();
  });

  it('should create an instance', () => {
    expect(agentlayoutModule).toBeTruthy();
  });
});
