import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceinfoComponent } from './insuranceinfo.component';

describe('InsuranceinfoComponent', () => {
  let component: InsuranceinfoComponent;
  let fixture: ComponentFixture<InsuranceinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
