import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentlayoutComponent } from './agentlayout.component';

describe('AgentlayoutComponent', () => {
  let component: AgentlayoutComponent;
  let fixture: ComponentFixture<AgentlayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentlayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentlayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
