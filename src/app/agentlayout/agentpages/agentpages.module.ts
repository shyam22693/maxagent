import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentpagesRoutingModule } from './agentpages-routing.module';
import { AgentpagesComponent } from './agentpages.component';

@NgModule({
  imports: [
    CommonModule,
    AgentpagesRoutingModule
  ],
  declarations: [AgentpagesComponent]
})
export class AgentpagesModule { }
