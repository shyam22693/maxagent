import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentpagesComponent } from './agentpages.component';

describe('AgentpagesComponent', () => {
  let component: AgentpagesComponent;
  let fixture: ComponentFixture<AgentpagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentpagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentpagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
