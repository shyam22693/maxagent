import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AgentService } from '../agent.service';
import { UsCountriesService } from './us-countries.service';
import { GlobalValueService } from './global-value.service';

import { AuthorizationService } from './customservices/authorization.service';
import { AdminauthorizationService } from './customservices/adminauthorization.service';

import { CookieService } from 'ngx-cookie-service';

import { AuthGuard } from './auth.guard';

import { AuthagentGuard } from './authagent.guard';
import { AuthhomeGuard } from './authhome.guard';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BsModalModule } from 'ng2-bs3-modal';
import { PdfViewerModule } from 'ng2-pdf-viewer';

// import { TagInputModule } from 'ngx-chips';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    BrowserAnimationsModule,
    NgbModule,
    ToastrModule.forRoot(), // ToastrModule added
    AppRoutingModule,
    BrowserModule,
    PdfViewerModule,
    AppRoutingModule,
    HttpClientModule,
    BsModalModule,
    // TagInputModule
    DeferLoadModule,
    NgSelectModule
  ],
  providers: [AgentService, AdminauthorizationService, CookieService,
    AuthorizationService, AuthGuard, AuthagentGuard, AuthhomeGuard, UsCountriesService, GlobalValueService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,

  ]
})
export class AppModule { }
