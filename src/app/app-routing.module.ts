import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', loadChildren: './layout/layout.module#LayoutModule'},
      { path: 'maf-admin', loadChildren: './adminlayout/adminlayout.module#AdminlayoutModule'},
      { path: 'maf-agent', loadChildren: './agentlayout/agentlayout.module#AgentlayoutModule'},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

 // useHash: true
  exports: [RouterModule]
})
export class AppRoutingModule { }
