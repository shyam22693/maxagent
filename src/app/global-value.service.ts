import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalValueService {

  s3Url: any = 'https://s3.amazonaws.com/mafserverless/';
  constructor() { }
}
