import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AdminauthorizationService } from './customservices/adminauthorization.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private auth: AdminauthorizationService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // handle any redirects if a user isn't authenticated
      var authenticatedUser = this.auth.getAuthenticatedUser();
      if (authenticatedUser == null) {
        // redirect the user
        this.router.navigate(['/maf-admin']);
        return false;
      }
      else{
      	return true;
      }
  }
}
