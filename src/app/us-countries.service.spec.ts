import { TestBed } from '@angular/core/testing';

import { UsCountriesService } from './us-countries.service';

describe('UsCountriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsCountriesService = TestBed.get(UsCountriesService);
    expect(service).toBeTruthy();
  });
});
