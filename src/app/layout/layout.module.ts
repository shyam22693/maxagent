import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { RegisterComponent } from './register/register.component';
import { PaymentComponent } from './payment/payment.component';
import { ThankuComponent } from './thanku/thanku.component';
import { AboutComponent } from './about/about.component';
import { BecomeagentComponent } from './becomeagent/becomeagent.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NgxPayPalModule } from '../../projects/ngx-paypal-lib/src/public_api';


import { NgxSpinnerModule } from 'ngx-spinner';
import { PaymentfailComponent } from './paymentfail/paymentfail.component';



@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPayPalModule,
    NgxSpinnerModule,
    NgbModule
  ],
  declarations: [LayoutComponent, HomeComponent, HeaderComponent, FooterComponent,
    RegisterComponent, PaymentComponent, ThankuComponent,
    AboutComponent, BecomeagentComponent, ContactusComponent, PaymentfailComponent, 
  ],
  exports: [HomeComponent, HeaderComponent,
    RegisterComponent, PaymentComponent, ThankuComponent,
    AboutComponent, BecomeagentComponent, ContactusComponent, PaymentfailComponent, 

  ]
})
export class LayoutModule { }
