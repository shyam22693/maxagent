import { Component, OnInit } from '@angular/core';
import { GlobalValueService } from './../../global-value.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  s3AssectUrl :any;
  public isCollapsed = false;
  constructor(private global:GlobalValueService) { }

  ngOnInit() {
    this.s3AssectUrl = `${this.global.s3Url}`;
  }
  collapse() {
    this.isCollapsed = !this.isCollapsed;
    const navbar = document.getElementsByTagName('nav')[0];
    if (this.isCollapsed) {
      navbar.classList.remove('navbar-transparent');
      navbar.classList.add('bg-white');
    } else {
      navbar.classList.add('navbar-transparent');
      navbar.classList.remove('bg-white');
    }

  }
}
