import { Component, OnInit } from '@angular/core';
import { GlobalValueService } from './../../global-value.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
s3AssectUrl:any;
  constructor(private global:GlobalValueService) { }

  ngOnInit() {
  	this.s3AssectUrl = `${this.global.s3Url}`;
  }

}
