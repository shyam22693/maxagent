import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AgentService } from '../../../agent.service';
import { CountriesService } from '../../../countries.service';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import * as crypto from 'crypto-js';

// import {usCountries} from './countries.json';
import { UsCountriesService } from './../../us-countries.service';



// import * as from '../../../assets/usCountries';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  textPattern: any;
  usCountries: any;
  stateInfo: any[] = [];
  countryInfo: any[] = [];
  cityInfo: any[] = [];


  public loader: Boolean = false;

  public submitButton: Boolean = false;

  encryptMode: boolean;
  registerPopup = false;
  passwordPattern: any;
  submitted = false;
  user: any;
  registerform: FormGroup;

  temp_sponserId: string;
  temp_AgentId: string;
  temp_firstName: string;
  countriesList = ['USA'];
  constructor(private countries: CountriesService,
    private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private agent: AgentService,
    private router: Router, private cookieService: CookieService, private uscountries: UsCountriesService) { }

  ngOnInit() {

    this.uscountries.getCountries().subscribe(res => {
      this.usCountries = res
      this.stateInfo = Object.keys(this.usCountries)
      console.log('this.uscountries', this.usCountries);
    });

    this.textPattern = '^[a-zA-Z ]*$';
    // this.getCountries();

    this.encryptMode = true;
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);


    // this.passwordPattern = '^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$';
    this.registerform = this.formBuilder.group({
      sponserId: [null, [Validators.required]],
      firstName: [null, [Validators.required, Validators.pattern(this.textPattern),
      Validators.minLength(3), Validators.maxLength(20)]],
      lastName: [null, [Validators.required, Validators.pattern(this.textPattern),
      Validators.minLength(2), Validators.maxLength(20)]],
      gender: [null, [Validators.required]],
      ssnNumber: [null, [Validators.required]],
      // phoneNo: [null, [Validators.required]],
      phoneNo: ['', Validators.compose([
        Validators.required, // Field is required
        Validators.minLength(10),
        Validators.maxLength(10),
      ])],
      emailId: [null, [Validators.required, Validators.email]],
      residentSuitApartment: [null, [Validators.required]],
      residentCity: [null, [Validators.required]],
      residentCountry: ['USA', [Validators.required]],
      residentState: [null, [Validators.required]],
      residentZipcode: [null, [Validators.required]],
      residentAddress: [null, [Validators.required]],
      isReadyPay: [null, [Validators.required]],
      isAgeVerified: [null, [Validators.required]],
      isAgreeNonRefund: [null, [Validators.required]]
    });
  }
  /** */
  // getCountries() {
  //   this.countries.allCountries().
  //     subscribe(
  //       data2 => {
  //         console.log('country list', data2);
  //         this.countryInfo = data2.Countries;
  //         // console.log('Data:', this.countryInfo);
  //       },
  //       err => console.log(err),
  //       () => console.log('complete')
  //     );
  // }

  // onChangeCountry(countryValue) {
  //   this.stateInfo = this.countryInfo[countryValue].States;
  //   this.cityInfo = this.stateInfo[0].Cities;
  //   console.log(this.cityInfo);
  // }

  onChangeState(stateValue) {
    // this.cityInfo = this.stateInfo[stateValue].Cities;
    this.cityInfo = this.usCountries[stateValue];
    // console.log(this.cityInfo);
  }

  get f() { return this.registerform.controls; }
  onRegister(user) {

    this.submitted = true;
    console.log('Register', this.user);
    if (this.registerform.invalid) {
      console.log('please fill all fields');
      return;
    }
    else {
      this.loader = true;
      this.submitButton = true;
    }

    this.user = {
      sponserId: this.registerform.get('sponserId').value.trim(),
      firstName: this.registerform.get('firstName').value.trim(),
      lastName: this.registerform.get('lastName').value.trim(),
      gender: this.registerform.get('gender').value.trim(),
      ssnNumber: this.registerform.get('ssnNumber').value.trim(),
      phoneNo: this.registerform.get('phoneNo').value.trim(),
      emailId: this.registerform.get('emailId').value.trim(),
      residentSuitApartment: this.registerform.get('residentSuitApartment').value.trim(),
      residentCity: this.registerform.get('residentCity').value.trim(),
      residentCountry: this.registerform.get('residentCountry').value.trim(),
      residentState: this.registerform.get('residentState').value.trim(),
      residentZipcode: this.registerform.get('residentZipcode').value.trim(),
      residentAddress: this.registerform.get('residentAddress').value.trim(),
      isReadyPay: this.registerform.get('isReadyPay').value,
      isAgeVerified: this.registerform.get('isAgeVerified').value,
      isAgreeNonRefund: this.registerform.get('isAgreeNonRefund').value,
    }

    console.log('cccc', this.user);

    this.agent.registerPost(this.user)
      .subscribe(res => {
        this.cookieService.set('AHJSKIDGTERCDHDFCSKDHDGDT', JSON.stringify(res.data.MainData));
        this.loader = false;
        // this.router.navigate(['/payment']);
        this.submitButton = false;
        location.href = res.data.MainData.DocuSignUrl;
      }, (err) => {
        console.log(err);
      });
  }

}
