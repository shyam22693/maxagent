import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalValueService } from './../../global-value.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
  showNavigationArrows = false;
  showNavigationIndicators = true;
  s3AssectUrl: any;

  constructor(config: NgbCarouselConfig, private router: Router, private global: GlobalValueService) {
    // customize default values of carousels used by this component tree
    config.interval = 5000;
    config.wrap = true;
    config.keyboard = true;
    config.pauseOnHover = false;

    // customize default values of carousels used by this component tree
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit() {
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxx', this.global.s3Url);
    this.s3AssectUrl = `${this.global.s3Url}`;
  }
  // Register Navigation
  register() {
    this.router.navigateByUrl('/register');
  }
}
