import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';

import { AuthGuard } from './../auth.guard';
import { AuthagentGuard } from './../authagent.guard';
import { AuthhomeGuard } from './../authhome.guard';

import { RegisterComponent } from './register/register.component';
import { PaymentComponent } from './payment/payment.component';
import { ThankuComponent } from './thanku/thanku.component';
import { AboutComponent } from './about/about.component';
import { BecomeagentComponent } from './becomeagent/becomeagent.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PaymentfailComponent } from './paymentfail/paymentfail.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'prefix' },
      { path: 'home', component: HomeComponent},
      { path: 'register', component: RegisterComponent },
      { path: 'payment', component: PaymentComponent },
      { path: 'thankyou', component: ThankuComponent },
      { path: 'aboutus', component: AboutComponent },
      { path: 'agent', component: BecomeagentComponent },
      { path: 'contactus', component: ContactusComponent },
      { path: 'paymentfail', component: PaymentfailComponent },
    
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
