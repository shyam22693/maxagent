import { Component, OnInit } from '@angular/core';
import { GlobalValueService } from './../../global-value.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
	s3AssectUrl:any;
  constructor(private global:GlobalValueService) { }

  ngOnInit() {
  	this.s3AssectUrl = `${this.global.s3Url}`;
  }

}
