import { TestBed } from '@angular/core/testing';

import { GlobalValueService } from './global-value.service';

describe('GlobalValueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalValueService = TestBed.get(GlobalValueService);
    expect(service).toBeTruthy();
  });
});
