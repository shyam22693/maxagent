import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from '../../customservices/adminauthorization.service';
import { AuthorizationService } from '../../customservices/authorization.service';


const poolData = {
  UserPoolId: 'us-east-1_mHJWVEl15', // Your user pool id here
  ClientId: '3ahsjgh8mrhf4dvgfsoujaaisl' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.scss'],
  providers: [NgbCarouselConfig]
})
export class AdmindashboardComponent implements OnInit {

  showNavigationArrows = false;
  showNavigationIndicators = true;
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  url = '';
  agentDetails: any;
  fileDataUri: string | ArrayBuffer;
  fileLicenceDataUri: string | ArrayBuffer;
  errorMsg: string;
  errorLicenceMsg: string;
  fileName: string;
  fileLicenceName: string;
  fileType: string;
  fileLicenceType: string;
  imageUpload: boolean;
  imageLicenceUpload: boolean;

  constructor(config: NgbCarouselConfig, private agent: AgentService, private agentAuth: AuthorizationService,
    private router: Router, private cookieService: CookieService, private activatedRoute: ActivatedRoute) {

    // customize default values of carousels used by this component tree
    config.interval = 5000;
    config.wrap = true;
    config.keyboard = true;
    config.pauseOnHover = false;

    // customize default values of carousels used by this component tree
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;

  }

  ngOnInit() {

  }



  agentLogout() {
    console.log('shyam');
    this.agentAuth.logOut();
    this.router.navigateByUrl('/home');
  }

}
