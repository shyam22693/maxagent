import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsurancecompanylistComponent } from './insurancecompanylist.component';

describe('InsurancecompanylistComponent', () => {
  let component: InsurancecompanylistComponent;
  let fixture: ComponentFixture<InsurancecompanylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsurancecompanylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsurancecompanylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
