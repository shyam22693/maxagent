import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AuthorizationService } from "../../customservices/authorization.service";

@Component({
  selector: 'app-insurancecompanylist',
  templateUrl: './insurancecompanylist.component.html',
  styleUrls: ['./insurancecompanylist.component.scss']
})
export class InsurancecompanylistComponent implements OnInit {
  allCompanyList: any;
  public loader: Boolean = true;
  constructor(public modalService: NgbModal, private agent: AgentService,
    private agentAuth: AuthorizationService, private router: Router,
    private cookieService: CookieService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.agent.getAllInsCompanyDetails()
      .subscribe(res => {
        console.log('this.agentDetail this.agentDetail', res.data.MainData)
        this.allCompanyList = res.data.MainData;
        this.loader = false;
      }, (err) => {
        console.log(err);
      });

  }

}
