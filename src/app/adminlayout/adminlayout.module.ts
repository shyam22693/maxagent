import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AdminlayoutRoutingModule } from './adminlayout-routing.module';
import { AdminlayoutComponent } from './adminlayout.component';

import { AdmindashboardComponent } from './admindashboard/admindashboard.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';

import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AgentslistComponent } from './agentslist/agentslist.component';
import { CreateinsurancecompanyComponent } from './createinsurancecompany/createinsurancecompany.component';
import { InsurancecompanylistComponent } from './insurancecompanylist/insurancecompanylist.component';
import { AssignagentComponent } from './assignagent/assignagent.component';

import { TagInputModule } from 'ngx-chips';
import { AgentPoliciesComponent } from './agent-policies/agent-policies.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    AdminlayoutRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    TagInputModule,
    Ng2SearchPipeModule,
    NgSelectModule
  ],
  declarations: [AdminlayoutComponent, AdmindashboardComponent,
    AdminLayoutComponent, AuthLayoutComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    AdminloginComponent,
    AgentslistComponent,
    CreateinsurancecompanyComponent,
    InsurancecompanylistComponent,
    AssignagentComponent,
    AgentPoliciesComponent,


  ],
  exports: [AdminLayoutComponent, AuthLayoutComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,

    AgentslistComponent,
    CreateinsurancecompanyComponent,
    InsurancecompanylistComponent,
    AssignagentComponent,
  ]
})
export class AdminlayoutModule { }
