import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminlayoutComponent } from './adminlayout.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';

import { AgentslistComponent } from './agentslist/agentslist.component';
import { CreateinsurancecompanyComponent } from './createinsurancecompany/createinsurancecompany.component';

import { InsurancecompanylistComponent } from './insurancecompanylist/insurancecompanylist.component';
import { AgentPoliciesComponent } from './agent-policies/agent-policies.component';


import { AssignagentComponent } from './assignagent/assignagent.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './../auth.guard';
import { AuthagentGuard } from './../authagent.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'admin-login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminlayoutComponent,
    children: [
      { path: '', redirectTo: 'admin-dashboard', pathMatch: 'prefix' },
      { path: 'admin-dashboard', component: AdmindashboardComponent, canActivate: [AuthGuard] },
      { path: 'agents-list', component: AgentslistComponent, canActivate: [AuthGuard] },
      { path: 'agent-policies-list', component: AgentPoliciesComponent, canActivate: [AuthGuard] },

      { path: 'create-insurance-company', component: CreateinsurancecompanyComponent, canActivate: [AuthGuard] },
      { path: 'insurance-company-list', component: InsurancecompanylistComponent, canActivate: [AuthGuard] },
      { path: 'assign-agent', component: AssignagentComponent, canActivate: [AuthGuard] },
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: 'admin-login' },
      { path: 'admin-login', component: AdminloginComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminlayoutRoutingModule { }
