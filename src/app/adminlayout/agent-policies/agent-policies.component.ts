import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';


@Component({
  selector: 'app-agent-policies',
  templateUrl: './agent-policies.component.html',
  styleUrls: ['./agent-policies.component.scss']
})
export class AgentPoliciesComponent implements OnInit {

  loader: Boolean = true;
  filter: Boolean = false;
  policy: any;
  totalPolicyData: any;
  allAgentsList: any;
  totalAgentsCommission: any;
  totalAgentsCompanyPool: any;
  totalAgents: any;
  totalAgentList: any;

  term: any;
  //  lcities: Array<{ id: number, text: string }> = [];
  lcities = [
    { id: 1, name: 'Vilnius' },
    { id: 2, name: 'Kaunas' },
    { id: 3, name: 'Pavilnys', disabled: true },
    { id: 4, name: 'Pabradė' },
    { id: 5, name: 'Klaipėda' }
  ];
  constructor(private cookieService: CookieService, private agent: AgentService, private router: Router) { }

  ngOnInit() {
    this.initialFunction();
  }


  initialFunction() {
    this.totalAgentsCommission = 0;
    this.totalAgentsCompanyPool = 0;
    let agt = [];

    this.agent.getAllPolocies()
      .subscribe(res => {
        console.log('Agent Policies Response', res.data.MainData.allagents.Items);
        // Agent Details Id and name
        this.getAgentIdNameList();
        this.totalPolicyData = res.data.MainData.allpolicies.Items;
        this.totalAgents = res.data.MainData.allagents.Items;
        // console.log('this.totalPolicyData this.totalPolicyData', this.totalPolicyData, this.totalAgents);
        if (this.totalPolicyData.length != 0) {
          this.totalPolicyData.map(ins => {
            this.totalAgentsCommission += ins.agentCommissionAmount;
            this.totalAgentsCompanyPool += ins.companyPoolAmount;
          });
        }

        if (this.totalAgents.length != 0) {
          this.totalAgents.map(ta => {
            agt.push({ 'agentId': ta.agentId, 'sponserId': ta.sponserId, 'firstName': ta.firstName })
          });
          this.totalAgentList = agt;
          //   console.log('this.totalAgentList this.totalAgentList', this.totalAgentList)
        }
        this.loader = false;
      },
        (err) => {
          console.log(err);
        });
  }
  // Pushing id and name

  getAgentIdNameList() {

  }
  removeFilter() {
    this.loader = true;
    this.initialFunction();
    this.filter = false;
  }

  getPoliciesByid(agent) {
    this.filter = true;
    this.loader = true;
    this.totalAgentsCommission = 0;
    this.totalAgentsCompanyPool = 0;
    let agt = [];
    console.log('agent agent', agent.target.value)

    let data = { agentId: agent.target.value };
    this.agent.getAllPolociesFilterById(data)
      .subscribe(res => {
        console.log('res res', res)
        this.totalPolicyData = res.data.MainData.agentAllPolicies
        this.totalAgents = this.totalAgents
        if (this.totalPolicyData.length != 0) {
          this.totalPolicyData.map(ins => {
            this.totalAgentsCommission += ins.agentCommissionAmount;
            this.totalAgentsCompanyPool += ins.companyPoolAmount;
          });
        }

        // if (this.totalAgents.length != 0) {
        //    this.totalAgents.map(ta => {
        //    	agt.push({ 'agentId': ta.agentId, 'sponserId': ta.sponserId,'firstName':ta.firstName })
        //    });
        //    this.totalAgentList = agt;
        //    console.log('this.totalAgentList this.totalAgentList',this.totalAgentList)
        //   }
        this.loader = false;
      },
        (err) => {
          console.log(err);
          this.loader = false
        });
  }
}