import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';
import { AuthorizationService } from '../../customservices/authorization.service';
import { DomSanitizer } from '@angular/platform-browser';

import swal from 'sweetalert2';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-assignagent',
  templateUrl: './assignagent.component.html',
  styleUrls: ['./assignagent.component.scss']
})
export class AssignagentComponent implements OnInit {
  public rejectModal: Boolean = false;
  public loader: Boolean = true;
  isAdminVerifiedApproved: any;
  public fail: Boolean = false;
  public activeBtn: Boolean = true;
  public rejectBtn: Boolean = true;
  public success: Boolean = false;
  public assignAgent: Boolean = false;
  public disassociatedAgent: Boolean = false;
  public reject: Boolean = false;
  public assigninsurancecompany: Boolean = false;
  closeResult: string;
  firstName: string;
  lastName: string;
  agentId: string;
  sponserId: string;
  emailId: string;
  phoneNo: string;
  residentAddress: string;
  residentCity: string;
  residentState: string;
  residentCountry: string;
  licence: string;
  LicenceURL: any;
  licenceThere: any;
  imageurl: any;
  type: any;

  showTable: boolean;
  submitted = false;
  rejectPopup = false;
  activeDisable: boolean = false;

  agentDetails: any;
  insuranceCompanies: any;
  rejectform: FormGroup;
  showSwal(type) {
    if (type == 'success') {
      swal({
        title: '',
        text: `Verification is Successful. Login Credentials Send via Email!`,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Username or Password Mismatch',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    } else if (type == 'NoLicence') {
      swal({
        title: '',
        text: 'No Licence Uploaded!',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
    else if (type == 'AgentAssociated') {
      swal({
        title: '',
        text: `The Insurance Company Successfully Associated for this Agent!`,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    }
    else if (type == 'AgentDissociated') {
      swal({
        title: '',
        text: 'The Insurance Company successfully Dissociated for This Agent!',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-warning'
      }).catch(swal.noop);
    } else if (type == 'rejectmailsent') {
      swal({
        title: '',
        text: 'Mail Sent Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    }
    else if (type == 'wentwrong') {
      swal({
        title: '',
        text: 'Something went Wrong.Please try Again!',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
  }
  constructor(private sanitizer: DomSanitizer, public modalService: NgbModal, private agent: AgentService,
    private agentAuth: AuthorizationService, private router: Router,
    private cookieService: CookieService, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) { }

  open(content, type) {
    if (type === 'sm') {
      console.log('aici');
      this.modalService.open(content, { size: 'sm' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    } else {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }


  public getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  ngOnInit() {
    this.assignAgentInitialize()
    this.rejectform = this.formBuilder.group({
      comments: [null, [Validators.required]],
      emailId: [null],
      agentId: [null],
    });
    // Verifying button

  }


  assignAgentInitialize() {
    let agentIdObj = JSON.parse(this.cookieService.get('ADUGBITAF'));
    let Aid = agentIdObj.agentId;
    this.agent.getAgentDetails(Aid)
      .subscribe(res => {
        console.log('res res', res);
        this.loader = false;
        this.agentDetails = res.data.Item;
        this.insuranceCompanies = res.data.InsurnaceCompanyList;
        this.firstName = this.agentDetails.firstName;
        this.lastName = this.agentDetails.lastName;
        this.agentId = this.agentDetails.agentId;
        this.sponserId = this.agentDetails.sponserId;
        this.emailId = this.agentDetails.emailId;
        this.phoneNo = this.agentDetails.phoneNo;
        this.residentAddress = this.agentDetails.residentAddress;
        this.residentCity = this.agentDetails.residentCity;
        this.residentState = this.agentDetails.residentState;
        this.residentCountry = this.agentDetails.residentCountry;
        this.isAdminVerifiedApproved = this.agentDetails.isAdminVerifiedApproved;
        this.licence = this.agentDetails.LicenceURI;
        this.licenceThere = this.licence ? true : false;
        this.imageurl = this.agentDetails.ImageURI != undefined ? this.agentDetails.ImageURI : null;

        this.assigninsurancecompany = this.isAdminVerifiedApproved;
        if (this.agentDetails.isAdminVerifiedRejected == true) {
          this.activeBtn = false;
          this.rejectBtn = false;
        }
        else {
          this.activeBtn = !this.isAdminVerifiedApproved;
          this.rejectBtn = !this.isAdminVerifiedApproved;
        }
      }, (err) => {
        console.log(err);
      });
  }


  makePassword() {
    var text = '';
    var possible = 'ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789';

    for (var i = 0; i < 9; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  onAgentAccepts(agentIds, firstName, emailId) {
    let passwords = this.makePassword();
    var data = { agentId: agentIds, firstName: firstName, emailId: emailId, password: passwords };
    console.log('data data', data);
    let email = data.emailId;
    let password = data.password;
    let agentid = data.agentId;
    this.agentAuth.register(email, password, agentid).subscribe(
      (output) => {
        this.onAccepting(data)
      },
      (err) => {
        console.log(err);
      });
  }

  onAccepting(data) {
    this.loader = true;
    this.agent.onAgentAccepted(data)
      .subscribe(res => {
        console.log('done!')
        this.loader = false;
        this.showSwal('success');
        this.assignAgentInitialize();
        this.success = true;
      }, (err) => {
        console.log(err);
        this.fail = true;
      });
  }

  // Reject Method

  onAgentReject() {
    this.rejectPopup = true;
  }
  rfclose() {
    this.rejectPopup = false;
  }

  // assignIns Click Event

  assignIns(agentId, insuranceCompanyId) {
    this.loader = true;
    let assigndata = { agentId: agentId, insuranceCompanyId: insuranceCompanyId }
    console.log('assigndata', assigndata);
    this.agent.onAgentAssign(assigndata)
      .subscribe(res => {
        console.log('done!')
        this.loader = false;
        this.showSwal('AgentAssociated');
        this.assignAgentInitialize();
      }, (err) => {
        console.log(err);
        this.showSwal('wentwrong');
        // alert('Something Went Wrong.Please try again later');
        this.fail = true;
      });
  }

  removeIns(agentId, insuranceCompanyId) {
    this.loader = true;
    console.log(agentId, insuranceCompanyId)
    let data = { agentId: agentId, insuranceCompanyId: insuranceCompanyId }

    console.log('data data', data);

    this.agent.onAgentDissociative(data)
      .subscribe(res => {
        console.log('done!')
        this.loader = false;
        this.showSwal('AgentDissociated');
        this.assignAgentInitialize();
        this.disassociatedAgent = false;
      }, (err) => {
        console.log(err);
        this.fail = true;
        this.showSwal('wentwrong');
      });

  }
  // Get form
  get f() { return this.rejectform.controls; }
  // Reject Form Submit
  onReject(email, agentid, firstname, user) {
    this.submitted = true;
    if (this.rejectform.invalid) {
      console.log('please fill all fields');
      return;
    }
    this.loader = true;
    console.log('user', email, agentid, firstname, user);
    let data = { emailId: email, agentId: agentid, firstName: firstname, comment: user.comments };
    console.log('data', data);
    if (data.emailId != null && data.agentId != null && data.firstName != null && data.comment != null) {
      this.regectSendMail(data);
    }
  }

  regectSendMail(data) {
    this.agent.onRejectEmail(data)
      .subscribe(res => {
        this.rejectModal = false;
        console.log('done!')
        //  this.reject = true;
        // alert('Email Had Been Sent To The Agent!');
        this.loader = false;
        this.showSwal('rejectmailsent');
        this.assignAgentInitialize();
      }, (err) => {
        console.log(err);
        // alert('Something Went Wrong.Please try again later');
        //  this.fail = true;
        this.loader = false;
        this.showSwal('wentwrong');
        this.assignAgentInitialize();
      });
  }
  // successAlert

  successAlert() {
    this.success = false;
  }
  assignAgentAlert() {
    this.assignAgent = false;
  }

  disassociatedAgentAlert() {
    this.disassociatedAgent = true;
  }
  rejectAlert() {
    this.reject = true;
  }
  // failAlert

  failAlert() {
    this.fail = false;
  }
  // 

  /**Download File */
  downloadFile() {
    console.log('shyam')
    const data = 'licence';
    const blob = new Blob([data], { type: 'application/octet-stream' });
    if (this.licence) {
      this.LicenceURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.licence);
    }
    else {
      this.showSwal('NoLicence');
    }
  }
  /** Modal */

  openrejectModal() {
    this.rejectModal = true;
  }
}
