import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { AdminauthorizationService } from '../../customservices/adminauthorization.service';

import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.scss']
})
export class AdminloginComponent implements OnInit {
  public loader: Boolean = false;
  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = '';
  submitted = false;
  user: any;
  adminloginform: FormGroup;
  bAuthenticated = false;

  public focus;
  public focus2;
  showSwal(type) {
    if (type == 'success') {
      swal({
        title: '',
        text: 'Login Successfully',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Username or Password Mismatch',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
  }
  constructor(private formBuilder: FormBuilder, private adminAuth: AdminauthorizationService, private _router: Router) { }

  ngOnInit() {

    var $page = document.getElementsByClassName('full-page')[0];
    var image_src;
    var image_container = document.createElement('div');
    image_container.classList.add('full-page-background');
    image_container.style.backgroundImage = 'url(assets/img/bg14.jpg)';
    $page.appendChild(image_container);
    $page.classList.add('login-page');

    this.adminloginform = this.formBuilder.group({
      emailId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });


    var authenticatedUser = this.adminAuth.getAuthenticatedUser();
    console.log('authenticatedUser', authenticatedUser)
    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }

  ngOnDestroy() {
    var $page = document.getElementsByClassName('full-page')[0];
    $page.classList.remove('login-page');
  }

  get ad() { return this.adminloginform.controls; }
  onLogin(user) {
    this.user = {
      emailId: this.adminloginform.get('emailId').value,
      password: this.adminloginform.get('password').value
    };

    console.log('cccc', this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.adminloginform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let email = this.user.emailId;
    let password = this.user.password;
    this.adminAuth.signIn(email, password).subscribe(
      (data) => {
        this.confirmCode = false;
        this.loader = true;
        this._router.navigateByUrl('/maf-admin/admin-dashboard');
      },
      (err) => {
        this.showSwal('fail');
        this.loader = false;

        this.error = 'Registration Error has occurred';
      }
    );
  }

}

