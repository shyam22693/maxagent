import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { TagInputModule } from 'ngx-chips';

import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AgentService } from '../../../agent.service';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-createinsurancecompany',
  templateUrl: './createinsurancecompany.component.html',
  styleUrls: ['./createinsurancecompany.component.scss']
})
export class CreateinsurancecompanyComponent implements OnInit {

  public loader: Boolean = false;
  public buttonSubmit: Boolean = false;
  ins: any;
  acceptedMimeTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/jpg'];

  @ViewChild('fileInput') fileInput: ElementRef;


  createInsuranceCompanyForm: FormGroup;
  submitted = false;
  insCompanies: any;
  url = '';
  agentDetails: any;
  fileDataUri: string | ArrayBuffer;
  fileLicenceDataUri: string | ArrayBuffer;
  errorMsg: string;
  errorLicenceMsg: string;
  fileName: string;
  fileLicenceName: string;
  fileType: string;
  fileLicenceType: string;
  base64File: string;
  iconUpload: boolean;
  imageLicenceUpload: boolean;

  showSwal(type) {
    if (type == 'success') {
      swal({
        title: '',
        text: 'insurance company has been created!',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    } else if (type == 'fail') {
      swal({
        title: '',
        text: 'Username or Password Mismatch',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
        buttonsStyling: false
      }).catch(swal.noop);
    }
  }
  constructor(private formBuilder: FormBuilder, private cookieService: CookieService,
    private agent: AgentService, private router: Router) { }

  ngOnInit() {
    this.createInsuranceCompanyForm = this.formBuilder.group({
      companyName: [null, [Validators.required]],
      companyProducts: [null, [Validators.required]],
      address: [null, [Validators.required]],
      city: [null, [Validators.required]],
      state: [null, [Validators.required]],
      country: [null, [Validators.required]],
      // fileInput: [null, [Validators.required]],

    //  iconUpload: [null, [Validators.required]],
    });
  }
  // Form Controls
  get f() {
    return this.createInsuranceCompanyForm.controls;
  }
  createInsCompany(ins) {

    this.submitted = true;
    this.buttonSubmit = true;
    if (this.createInsuranceCompanyForm.invalid) {
      console.log('please fill all fields');
      return;
    }
    this.loader = true;
    this.insCompanies = {
      companyName: this.createInsuranceCompanyForm.get('companyName').value.trim(),
      address: this.createInsuranceCompanyForm.get('address').value.trim(),
      city: this.createInsuranceCompanyForm.get('city').value.trim(),
      state: this.createInsuranceCompanyForm.get('state').value.trim(),
      country: this.createInsuranceCompanyForm.get('country').value.trim(),
      companyProducts: this.createInsuranceCompanyForm.get('companyProducts').value,
      base64File: this.fileDataUri.toString().split(',')[1],
      fileName: this.fileName,
      fileType: this.fileType,
      iconUpload: this.iconUpload
    };
    console.log('this.insCompanies', this.insCompanies);

    this.agent.createInsCompany(this.insCompanies)
      .subscribe(res => {
        this.loader = true;
        // this.showSwal('success');
        //  alert('insurance company has been created!');
        this.router.navigateByUrl('/maf-admin/insurance-company-list');
      }, (err) => {
        console.log(err);
      });
  }

  previewFile() {
    const file = this.fileInput.nativeElement.files[0];
    if (file && this.validateFile(file)) {
      this.fileName = file.name;
      this.fileType = file.type;
      this.iconUpload = true;
      const reader = new FileReader();
      reader.readAsDataURL(this.fileInput.nativeElement.files[0]);
      reader.onload = () => {
        this.fileDataUri = reader.result;
        this.base64File = this.fileDataUri.toString().split(',')[1];
        console.log('this.fileDataUri', this.fileDataUri, this.base64File, this.fileName, this.iconUpload);

      }
    } else {
      this.errorMsg = 'File must be jpg, jpeg, or png and cannot be exceed 500 KB in size'
    }
  }


  validateFile(file) {
    return this.acceptedMimeTypes.includes(file.type) && file.size < 5000000;
  }
}
