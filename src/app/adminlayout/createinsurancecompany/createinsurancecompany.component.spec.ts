import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateinsurancecompanyComponent } from './createinsurancecompany.component';

describe('CreateinsurancecompanyComponent', () => {
  let component: CreateinsurancecompanyComponent;
  let fixture: ComponentFixture<CreateinsurancecompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateinsurancecompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateinsurancecompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
