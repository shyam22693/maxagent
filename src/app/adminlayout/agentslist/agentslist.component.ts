import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from "../../customservices/adminauthorization.service";

@Component({
  selector: 'app-agentslist',
  templateUrl: './agentslist.component.html',
  styleUrls: ['./agentslist.component.scss']
})
export class AgentslistComponent implements OnInit {

  public loader: Boolean = true;
  public agents = [];
  agentId: any;


  constructor(private formBuilder: FormBuilder, private agent: AgentService,
    private adminAuth: AdminauthorizationService, private router: Router, 
    private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.agent.getAllAgent()
      .subscribe(res => {
        this.agents = res.data.MainData.Items;
        this.loader = false;
        console.log('agents', this.agents);
      }, (err) => {
        console.log(err);
      });
  }

  getAgentById(agentId) {
    console.log('agentid', agentId);
    this.agentId = { agentId: agentId };
    // this.router.navigate(['/maf-admin/assign-agent', agentId]);
    this.cookieService.set('ADUGBITAF', JSON.stringify(this.agentId));
    console.log(JSON.parse(this.cookieService.get('ADUGBITAF')))
    this.router.navigateByUrl('/maf-admin/assign-agent');
  }

  adminLogout() {
    console.log('shyam');
    this.adminAuth.logOut();
    this.router.navigateByUrl('/admin');
  }

}
