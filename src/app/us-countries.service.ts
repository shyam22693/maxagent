import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsCountriesService {

  constructor(private httpService: HttpClient) { }

  getCountries() {
    return this.httpService.get('./assets/importantFiles/usCountries.json');
  }

}
