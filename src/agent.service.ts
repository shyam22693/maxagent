import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const httpHeaders = new HttpHeaders({
  'content-Type': 'application/json'
});

const httpOptions = {
  headers: httpHeaders
};


@Injectable({
  providedIn: 'root'
})
export class AgentService {
  public data;
  public apiUrl = 'https://q4ph7hsmg6.execute-api.us-east-1.amazonaws.com/realstage/';

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }
  /** Register Service Call */

  registerPost(data): Observable<any> {
    return this.http.post(this.apiUrl + 'agent/registration', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Payment SAVE LOGIC;
  postPayment(data): Observable<any> {
    return this.http.post(this.apiUrl + 'agent/register-email', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
// Get All Agents

  getAllAgent(): Observable<any> {
    return this.http.get(this.apiUrl + 'maf-admin/agents-section', httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
// Agent Details

  getAgentDetails(agentId): Observable<any> {
    return this.http.get(this.apiUrl + 'maf-admin/agent-by-id/' + agentId, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

// Agent accepted

  onAgentAccepted(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/accept-the-agent', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  onRejectEmail(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/reject-the-agent', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  sendEnquiry(data): Observable<any> {
    return this.http.post(this.apiUrl + 'agent/contactus', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  getAgentProfile(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/agent-profile', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  

  getAllPolociesFilterById(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/get-policy-data-by-id', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  //After agent login
  writePolicy(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/write-policy', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  getPolocies(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/policies-list', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  

  getAllPolocies(): Observable<any> {
    return this.http.get(this.apiUrl + 'maf-admin/all-policies-list', httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  onAgentAssign(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/assign-agent-insurance-company', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  uploadProfilePic(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/upload-agent-profile-pic', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  onAgentDissociative(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/dissociate-agent', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  uploadAgentLicence(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/upload-agent-licence', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  createInsCompany(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-admin/insurance-company', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

   EandOPostPayment(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/monthly-insurance-payment/agent-first-month-payment', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  EandOAgentPaymentList(data): Observable<any> {
    return this.http.post(this.apiUrl + 'maf-agent/monthly-insurance-payment/agent-payment-list', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAllInsCompanyDetails(): Observable<any> {
    return this.http.get(this.apiUrl + '/maf-admin/insurance-company', httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAllInsOnlog(agentId): Observable<any> {
    return this.http.get(this.apiUrl + '/maf-agent/insurance-company-list/' + agentId, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
